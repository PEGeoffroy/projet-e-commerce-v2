<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CartLineRepository")
 */
class CartLine
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $product;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cart", inversedBy="relation")
     * @ORM\JoinColumn(nullable=false)
     */
    private $cart_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="cartLines_id")
     * @ORM\JoinColumn(nullable=false)
     */
    private $relation;

    public function getId()
    {
        return $this->id;
    }

    public function getProduct(): ?int
    {
        return $this->product;
    }

    public function setProduct(int $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCartId(): ?Cart
    {
        return $this->cart_id;
    }

    public function setCartId(?Cart $cart_id): self
    {
        $this->cart_id = $cart_id;

        return $this;
    }

    public function getRelation(): ?Product
    {
        return $this->relation;
    }

    public function setRelation(?Product $relation): self
    {
        $this->relation = $relation;

        return $this;
    }
}
