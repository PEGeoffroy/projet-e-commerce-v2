<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class EditProductController extends Controller
{
    /**
     * @Route("/edit/product", name="edit_product")
     */
    public function index()
    {
        return $this->render('edit_product/index.html.twig', [
            'controller_name' => 'EditProductController',
        ]);
    }
}
